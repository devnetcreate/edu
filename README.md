# Roles: 🏄
  - Enrollment : Create a front end to validate and enroll students (names) and devices (mac addresses)
  - Portal: Create a front end for teachers to login and view students and assignments. 
  - Location : Use Meraki Scanning API to validate student is in class currently
  - Assigments : Collect and store file attachments from validated students using spark messages api
  - Hub : Tie the app together via REST api or shared mongo db
  - Expert: **Ashley Roach**

  ![](whiteboard.png)