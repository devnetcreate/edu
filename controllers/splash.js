const spark = require('./spark');
const express = require('express');

var request = require("request");



exports.splash = (req, res) => {
    res.render('splash', {
      title: 'Welcome to Camp Create EDU',
      ip_address: req.query.client_ip,
      mac_address: req.query.client_mac,
      ap_mac: req.query.node_mac,
      base_url: req.query.base_grant_url,
      redirect_url: req.query.user_continue_url
    });
};

exports.postSplash = (req, res, next) => {
    req.assert('client_email', 'Email is not valid').isEmail();
    req.sanitize('client_email').normalizeEmail({ gmail_remove_dots: false });

    const errors = req.validationErrors();


    if (errors) {
        req.flash('errors', errors);
        return res.redirect(req.originalUrl);
    }
    
    spark.memberships({
        "email": req.body.client_email,
        "mac_address": req.body.client_mac
    })

    //const Attendance = new AttendanceRecord({
    //    email: req.body.client_email,
    //    mac_accress: req.body.mac_address
    //  });

      res.redirect(req.query.base_grant_url + '?continue_url='  + req.query.user_continue_url + '&duration=60');
    
      console.log(req.query.base_grant_url + '?continue_url='  + req.query.user_continue_url + '&duration=60');
};


function Test(message){
    console.log("Message Received: " + message);
} 


function addDeviceRecord(email, mac_address, req)
{
  

    var BaseURL = req.headers.host;
    console.log("URL:" + URL);


    var options = { method: 'POST',
      url: BaseURL + '/assignMacAddress',
      headers: 
       {
         'Content-Type': 'application/json' },
      body: 
       { email: email,
         macAddress: mac_address},
         json: true };
    
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
    
      console.log(body);
    });
    
}





function CheckUserImage(email, image){

    var data = JSON.stringify({
        "email": email,
        "photo": image
    });
  
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
  
    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
        console.log(this.responseText);
        }
    });
  
    xhr.open("POST", "https://devnetcreate.azurewebsites.net/api/facedetection");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Cache-Control", "no-cache");
    xhr.setRequestHeader("Postman-Token", "2109348b-a567-e62a-2651-f70a741dd8ac");
  
    return xhr.send(data);

}
